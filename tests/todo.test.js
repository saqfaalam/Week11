const app = require("../app");
const request = require('supertest');

describe("API /todos", () => {

    it("test get /todos", (done) => {
        request(app)
            .get("/todos")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                console.log(respon.body)
                const firstData = response.body[0]
                expect(firstData.id).toBe(1)
                expect(firstData.title).toBe('Instagram')
                expect(firstData.status).toBe("active")
                done();
            })
            .catch(done)
    })

    it("test get /todos/:id", (done) => {
        
        request(app)
            .get("/todos/2")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data.id).toBe(2)
                expect(data.title).toBe('Tiktok')
                expect(data.status).toBe('inactive')
                done();
            })
            .catch(done)
    })

    it("test post /todos", (done) => {

        request(app)
            .post("/todos")
            .send({ title: "Twitter", status: "active"})
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .then(response => {
                const data = response.body;
                expect(data.title).toBe("Twitter")
                expect(data.status).toBe("active")
                done();
            })
            .catch(done)
    })

    it("delete post /todos/:id", (done) => {

        request(app)
            .delete("/todos/3")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data.message).toBe("Delete successfully")
                done();
            })
            .catch(done)
    })
    
})
