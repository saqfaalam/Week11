const express = require("express");
const TodoController = require("../controllers/todoController");
const router = express.Router();

router.get("/todos", TodoController.findAll);
router.get("/todos/:id", TodoController.findOne);
router.post("/todos", TodoController.create);
router.delete("/todos/:id", TodoController.destroy);
module.exports = router